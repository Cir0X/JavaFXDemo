# JavaFX Demo

This is just a small JavaFX with gradle demo.

## Build

```bash
./gradlew shadowJar
```

## Usage

```bash
java -jar build/libs/JavaFXDemo-1.0-SNAPSHOT-all.jar
```
